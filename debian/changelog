globus-gram-job-manager-scripts (7.3-1) unstable; urgency=medium

  * Add man pages
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Mon, 14 Dec 2020 21:24:19 +0100

globus-gram-job-manager-scripts (7.2-1) unstable; urgency=medium

  * Repair broken perlmoduledir definition in globus-job-manager-script.pl

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Mon, 02 Sep 2019 14:56:12 +0200

globus-gram-job-manager-scripts (7.1-3) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:37 +0200

globus-gram-job-manager-scripts (7.1-2) unstable; urgency=medium

  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:08:29 +0100

globus-gram-job-manager-scripts (7.1-1) unstable; urgency=medium

  * Architecture independent package should not depend on libtool

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 16 Nov 2018 14:06:46 +0100

globus-gram-job-manager-scripts (7.0-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:31 +0200

globus-gram-job-manager-scripts (6.10-1) unstable; urgency=medium

  * GT6 update: Fix regex for perl 5.26 compatibility
  * Drop patch globus-gram-job-manager-scripts-perl-5.26.patch (accepted
    upstream)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 22 Oct 2017 20:03:58 +0200

globus-gram-job-manager-scripts (6.9-3) unstable; urgency=medium

  * Adapt to perl 5.26 syntax (Closes: #870345)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 11 Aug 2017 17:03:27 +0200

globus-gram-job-manager-scripts (6.9-2) unstable; urgency=medium

  * Packaging updates

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:32 +0200

globus-gram-job-manager-scripts (6.9-1) unstable; urgency=medium

  * GT6 update: Updated man pages

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 11 Nov 2016 13:43:04 +0100

globus-gram-job-manager-scripts (6.8-1) unstable; urgency=medium

  * GT6 update
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sat, 03 Sep 2016 03:46:16 +0200

globus-gram-job-manager-scripts (6.7-4) unstable; urgency=medium

  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 14:50:10 +0100

globus-gram-job-manager-scripts (6.7-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 22:57:14 +0100

globus-gram-job-manager-scripts (6.7-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 11:38:43 +0100

globus-gram-job-manager-scripts (6.7-1) unstable; urgency=medium

  * GT6 update
  * Includes improvements from Open Science Grid (OSG)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 Oct 2014 06:25:30 +0100

globus-gram-job-manager-scripts (6.6-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 24 Sep 2014 19:48:41 +0200

globus-gram-job-manager-scripts (5.0-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.5
  * Implement Multi-Arch support

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 11 Nov 2013 15:46:28 +0100

globus-gram-job-manager-scripts (4.2-3) unstable; urgency=low

  * Remove DM Allowed from debian/control

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 May 2013 05:54:26 +0200

globus-gram-job-manager-scripts (4.2-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 23:23:44 +0100

globus-gram-job-manager-scripts (4.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 30 Dec 2011 11:55:38 +0100

globus-gram-job-manager-scripts (2.12-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.4

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 06 Jun 2011 17:13:00 +0200

globus-gram-job-manager-scripts (2.11-2) unstable; urgency=low

  * Add README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 27 Apr 2011 09:37:43 +0200

globus-gram-job-manager-scripts (2.11-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.2

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 19 Jul 2010 14:15:49 +0200

globus-gram-job-manager-scripts (2.5-2) unstable; urgency=low

  * Converting to package format 3.0 (quilt)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:29 +0200

globus-gram-job-manager-scripts (2.5-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.1

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 15 Apr 2010 18:29:49 +0200

globus-gram-job-manager-scripts (2.4-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 28 Jan 2010 00:30:46 +0100

globus-gram-job-manager-scripts (0.7-1) unstable; urgency=low

  * Initial release (Closes: #540276).

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Aug 2009 14:40:48 +0200
